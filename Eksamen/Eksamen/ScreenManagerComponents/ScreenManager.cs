﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScreenManager.cs" company="Håkon Martin Eide">
//   Håkon Martin Eide
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen.ScreenManagerComponents
{
    using System.Collections.Generic;
    using System.Diagnostics;

    using Eksamen.GameScreens;

    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// The screen manager.
    /// The screen manager will store a list of all screens and run the 
    /// one at the end of the list.
    /// It will check if screens are transitioning or if a screen is a popup on top of another.
    /// </summary>
    public class ScreenManager : DrawableGameComponent
    {
        #region Fields

        /// <summary>
        /// The screens.
        /// </summary>
        private readonly List<GameScreen> screens = new List<GameScreen>();

        /// <summary>
        /// The screens to update.
        /// </summary>
        private readonly List<GameScreen> screensToUpdate = new List<GameScreen>();

        /// <summary>
        /// The blank texture.
        /// </summary>
        private Texture2D blankTexture;

        /// <summary>
        /// The content.
        /// </summary>
        private ContentManager content;

        /// <summary>
        /// The font.
        /// </summary>
        private SpriteFont font;

        /// <summary>
        /// The input handler.
        /// </summary>
        private InputHandler inputHandler;

        /// <summary>
        /// The is initialized.
        /// </summary>
        private bool isInitialized;

        /// <summary>
        /// The sprite batch.
        /// </summary>
        private SpriteBatch spriteBatch;

        /// <summary>
        /// The trace enabled.
        /// Will list all screens currently in the screens list in the debug window
        /// </summary>
        private bool traceEnabled;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ScreenManager"/> class.
        /// </summary>
        /// <param name="game">
        /// The game.
        /// </param>
        public ScreenManager(Game game)
            : base(game)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the ContentManager from the gamescreen.
        /// </summary>
        public ContentManager Content
        {
            get
            {
                return this.content;
            }
        }

        /// <summary>
        /// Gets Font.
        /// </summary>
        public SpriteFont Font
        {
            get
            {
                return this.font;
            }
        }

        /// <summary>
        /// Gets InputHandler.
        /// </summary>
        public InputHandler InputHandler
        {
            get
            {
                return this.inputHandler;
            }
        }

        /// <summary>
        /// Propertiesene i screen manager er slik at screens kan hente inn samme spriteBatch og input handler etc
        /// uten å måtte newe.
        /// </summary>
        public SpriteBatch SpriteBatch
        {
            get
            {
                return this.spriteBatch;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether TraceEnabled.
        /// Used for debugging the screen
        /// </summary>
        public bool TraceEnabled
        {
            get
            {
                return this.traceEnabled;
            }

            set
            {
                this.traceEnabled = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Denne metoden vil legge til en skjerm i screens listen, som er listen over alle aktive GameScreens.
        /// Den vil først gjøre en sjekk for om vi allerede har opprettet denne skjermen i listen, og hvis vi har det vil den flytte denne skjermen
        /// til slutten av listen, og det er da den som er aktiv.
        /// </summary>
        /// <param name="screen">
        /// </param>
        public void AddScreen(GameScreen screen)
        {
            screen.IsExiting = false;

            if (this.isInitialized)
            {
                screen.LoadContent();
            }

            int screenIndex = this.screens.FindIndex(f => f.GetType() == screen.GetType());

            if (screenIndex > -1)
            {
                // Debug.WriteLine("Matching Screen");
                GameScreen currentScreenEntry = this.screens[screenIndex];
                currentScreenEntry.IsExiting = false;
                currentScreenEntry.ScreenState = ScreenState.Active;
                this.screens.RemoveAt(screenIndex);
                this.screens.Add(currentScreenEntry);
            }
            else
            {
                screen.Initialize();
                this.screens.Add(screen);
            }
        }

        /// <summary>
        /// The draw.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        public override void Draw(GameTime gameTime)
        {
            foreach (GameScreen screen in this.screens)
            {
                if (screen.ScreenState == ScreenState.Hidden)
                {
                    continue;
                }

                screen.Draw(gameTime);
            }
        }

        /// <summary>
        /// The fade back to black.
        /// </summary>
        /// <param name="alpha">
        /// The alpha.
        /// </param>
        public void FadeBackToBlack(float alpha)
        {
            Viewport viewport = this.GraphicsDevice.Viewport;

            this.spriteBatch.Begin();

            this.spriteBatch.Draw(
                this.blankTexture, new Rectangle(0, 0, viewport.Width, viewport.Height), Color.Black * alpha);

            this.spriteBatch.End();
        }

        /// <summary>
        /// The initialize.
        /// </summary>
        public override void Initialize()
        {
            this.content = this.Game.Content;
            base.Initialize();
            this.isInitialized = true;
        }

        /// <summary>
        /// The remove newest screen.
        /// </summary>
        public void RemoveNewestScreen()
        {
            this.RemoveScreen(this.screens[this.screens.Count - 1]);
        }

        /// <summary>
        /// The remove screen.
        /// </summary>
        /// <param name="screen">
        /// The screen.
        /// </param>
        public void RemoveScreen(GameScreen screen)
        {
            if (this.isInitialized)
            {
                screen.UnloadContent();
            }

            this.screens.Remove(screen);
            this.screensToUpdate.Remove(screen);
            if (this.screens.Count < 1)
            {
                this.Game.Exit();
            }
        }

        /// <summary>
        /// The return to menu.
        /// </summary>
        public void ReturnToMenu()
        {
            this.screens[this.screens.Count - 1].ExitScreen();
            for (int i = this.screens.Count - 1; i > 0; i--)
            {
                this.RemoveScreen(this.screens[i]);
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        public override void Update(GameTime gameTime)
        {
            // legger inn alle screensene i update listen.
            this.screensToUpdate.Clear();
            foreach (GameScreen screen in this.screens)
            {
                this.screensToUpdate.Add(screen);
            }

            bool otherScreenHasFocus = !this.Game.IsActive;
            bool coveredByOtherScreen = false;

            // En while loop som teller ned antall screens å sjekke for updates.
            while (this.screensToUpdate.Count > 0)
            {
                // Setter den skjermen den skal jobbe med nå
                GameScreen screen = this.screensToUpdate[this.screensToUpdate.Count - 1];

                // Fjerner samme skjermen fra listen slik at den ikke er med i neste løkker
                this.screensToUpdate.RemoveAt(this.screensToUpdate.Count - 1);

                // Update screen
                screen.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
                if (screen.ScreenState == ScreenState.TransitionOn || screen.ScreenState == ScreenState.Active)
                {
                    if (!otherScreenHasFocus)
                    {
                        this.inputHandler.Update(gameTime);
                        screen.HandleInput(this.InputHandler);
                        otherScreenHasFocus = true;
                    }

                    if (!screen.IsPopup)
                    {
                        coveredByOtherScreen = true;
                    }
                }
            }

            if (this.traceEnabled)
            {
                this.TraceScreens();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The load content.
        /// </summary>
        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(this.GraphicsDevice);
            this.blankTexture = this.content.Load<Texture2D>("blank");
            this.font = this.content.Load<SpriteFont>("Font");
            this.inputHandler = new InputHandler();

            // tell screens to load content
            foreach (GameScreen screen in this.screens)
            {
                screen.LoadContent();
            }

            base.LoadContent();
        }

        /// <summary>
        /// The unload content.
        /// </summary>
        protected override void UnloadContent()
        {
            foreach (GameScreen screen in this.screens)
            {
                screen.UnloadContent();
            }
        }

        /// <summary>
        /// The trace screens.
        /// </summary>
        private void TraceScreens()
        {
            var screenNames = new List<string>();

            foreach (GameScreen screen in this.screens)
            {
                screenNames.Add(screen.GetType().Name);
            }

            Debug.WriteLine(string.Join(", ", screenNames.ToArray()));
        }

        #endregion
    }
}