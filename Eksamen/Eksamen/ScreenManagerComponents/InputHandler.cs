﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="Håkon Martin Eide" file="InputHandler.cs">
//   Håkon Martin Eide
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen.ScreenManagerComponents
{
    using System.Diagnostics;

    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Input;

    /// <summary>
    /// The input handler provide easy
    /// methods to check for user input aswell as
    /// having pre-defined checks for certain actions
    /// like confirming or declinging an action.
    /// I chose to implement this in this project as it is already well integrated
    /// with my screenManager implementation.
    /// </summary>
    public class InputHandler
    {
#if WINDOWS

        /// <summary>
        /// Gets or sets KeyBoardState.
        /// </summary>
        public KeyboardState KeyBoardState { get; protected set; }

        /// <summary>
        /// Gets or sets PreviousKeyboardState.
        /// </summary>
        public KeyboardState PreviousKeyboardState { get; protected set; }
#endif
#if WINDOWS || XBOX

        /// <summary>
        /// Gets or sets GamepadState.
        /// </summary>
        public GamePadState GamepadState { get; protected set; }

        /// <summary>
        /// Gets or sets PreviousGamepadState.
        /// </summary>
        public GamePadState PreviousGamepadState { get; protected set; }

        /// <summary>
        /// The player index.
        /// </summary>
        public PlayerIndex PlayerIndex = PlayerIndex.One;

        /// <summary>
        /// The accept button.
        /// </summary>
        public Buttons AcceptButton = Buttons.A;

        /// <summary>
        /// The decline button.
        /// </summary>
        public Buttons DeclineButton = Buttons.B;
#endif

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        public void Update(GameTime gameTime)
        {
#if WINDOWS
            this.PreviousKeyboardState = this.KeyBoardState;
            this.KeyBoardState = Keyboard.GetState();
#endif
#if WINDOWS || XBOX
            this.PreviousGamepadState = this.GamepadState;
            this.GamepadState = GamePad.GetState(this.PlayerIndex);
#endif
        }

        #region Keyboard

#if WINDOWS

        /// <summary>
        /// The key pressed.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The key pressed.
        /// </returns>
        public bool KeyPressed(Keys key)
        {
            return this.KeyBoardState.IsKeyDown(key) && this.PreviousKeyboardState.IsKeyUp(key);
        }

        /// <summary>
        /// The key released.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The key released.
        /// </returns>
        public bool KeyReleased(Keys key)
        {
            return this.KeyBoardState.IsKeyUp(key) && this.PreviousKeyboardState.IsKeyDown(key);
        }

        /// <summary>
        /// The key down.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The key down.
        /// </returns>
        public bool KeyDown(Keys key)
        {
            return this.KeyBoardState.IsKeyDown(key);
        }

        /// <summary>
        /// The key up.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The key up.
        /// </returns>
        public bool KeyUp(Keys key)
        {
            return this.KeyBoardState.IsKeyUp(key);
        }

        // Moving sjekkene blir brukt for å bestemnme om vi trykker en av tastene som gjør at vi skal bevege oss i en bestemt rettning.
        /// <summary>
        /// The moving up.
        /// </summary>
        /// <returns>
        /// The moving up.
        /// </returns>
        public bool MovingUp()
        {
            if (this.ButtonDown(Buttons.DPadUp) || this.KeyDown(Keys.Up) || (this.GamepadState.ThumbSticks.Left.Y > 0))
            {
#if DEBUG
                Debug.WriteLine("Moving Up");
#endif
                return true;
            }

            return false;
        }

        /// <summary>
        /// The moving down.
        /// </summary>
        /// <returns>
        /// The moving down.
        /// </returns>
        public bool MovingDown()
        {
            if (this.ButtonDown(Buttons.DPadDown) || this.KeyDown(Keys.Down)
                || (this.GamepadState.ThumbSticks.Left.Y < 0))
            {
#if DEBUG
                Debug.WriteLine("Moving Down");
#endif
                return true;
            }

            return false;
        }

        /// <summary>
        /// The moving left.
        /// </summary>
        /// <returns>
        /// The moving left.
        /// </returns>
        public bool MovingLeft()
        {
            if (this.ButtonDown(Buttons.DPadLeft) || this.KeyDown(Keys.Left)
                || (this.GamepadState.ThumbSticks.Left.X < 0))
            {
#if DEBUG
                Debug.WriteLine("Moving Left");
#endif
                return true;
            }

            return false;
        }

        /// <summary>
        /// The moving right.
        /// </summary>
        /// <returns>
        /// The moving right.
        /// </returns>
        public bool MovingRight()
        {
            if (this.ButtonDown(Buttons.DPadRight) || this.KeyDown(Keys.Right)
                || (this.GamepadState.ThumbSticks.Left.X > 0))
            {
#if DEBUG
                Debug.WriteLine("Moving Right");
#endif
                return true;
            }

            return false;
        }

        /// <summary>
        /// The pressed up.
        /// </summary>
        /// <returns>
        /// The pressed up.
        /// </returns>
        public bool PressedUp()
        {
            if (this.ButtonPressed(Buttons.DPadUp) || this.KeyPressed(Keys.Up))
            {
#if DEBUG
                Debug.WriteLine("Moving Up");
#endif
                return true;
            }

            return false;
        }

        /// <summary>
        /// The pressed down.
        /// </summary>
        /// <returns>
        /// The pressed down.
        /// </returns>
        public bool PressedDown()
        {
            if (this.ButtonPressed(Buttons.DPadDown) || this.KeyPressed(Keys.Down))
            {
#if DEBUG
                Debug.WriteLine("Moving Down");
#endif
                return true;
            }

            return false;
        }

        // Sjekker om vi har trykket på confirm eller pause knappen i spillet. Slipper å skrive to sjekker andre steder i koden.
        /// <summary>
        /// The confirm button pressed.
        /// </summary>
        /// <returns>
        /// The confirm button pressed.
        /// </returns>
        public bool ConfirmButtonPressed()
        {
            if (this.ButtonPressed(Buttons.A) || this.KeyPressed(Keys.Space))
            {
#if DEBUG
                Debug.WriteLine("Confirm Button Pressed");
#endif
                return true;
            }

            return false;
        }

        /// <summary>
        /// The pause button pressed.
        /// </summary>
        /// <returns>
        /// The pause button pressed.
        /// </returns>
        public bool PauseButtonPressed()
        {
            return this.ButtonPressed(Buttons.Start) || this.KeyPressed(Keys.Escape);
        }

#endif

        #endregion

        #region Gamepad

#if WINDOWS || XBOX

        /// <summary>
        /// The button pressed.
        /// </summary>
        /// <param name="button">
        /// The button.
        /// </param>
        /// <returns>
        /// The button pressed.
        /// </returns>
        public bool ButtonPressed(Buttons button)
        {
            return this.GamepadState.IsButtonDown(button) && this.PreviousGamepadState.IsButtonUp(button);
        }

        /// <summary>
        /// The button released.
        /// </summary>
        /// <param name="button">
        /// The button.
        /// </param>
        /// <returns>
        /// The button released.
        /// </returns>
        public bool ButtonReleased(Buttons button)
        {
            return this.GamepadState.IsButtonUp(button) && this.PreviousGamepadState.IsButtonUp(button);
        }

        /// <summary>
        /// The button down.
        /// </summary>
        /// <param name="button">
        /// The button.
        /// </param>
        /// <returns>
        /// The button down.
        /// </returns>
        public bool ButtonDown(Buttons button)
        {
            return this.GamepadState.IsButtonDown(button);
        }

        /// <summary>
        /// The button up.
        /// </summary>
        /// <param name="button">
        /// The button.
        /// </param>
        /// <returns>
        /// The button up.
        /// </returns>
        public bool ButtonUp(Buttons button)
        {
            return this.GamepadState.IsButtonUp(button);
        }

#endif

        // Er de samme som for windows bare uten Keyboard sjekkene.
#if XBOX

    // <summary>
    // The moving up.
    /// </summary>
    /// <returns>
    /// The moving up.
    /// </returns>
        public bool MovingUp()
        {
            if (this.ButtonDown(Buttons.DPadUp) || (this.GamepadState.ThumbSticks.Left.Y > 0))
            {
#if DEBUG
                Debug.WriteLine("Moving Up");
#endif
                return true;
            }

            return false;
        }

        /// <summary>
        /// The moving down.
        /// </summary>
        /// <returns>
        /// The moving down.
        /// </returns>
        public bool MovingDown()
        {
            if (this.ButtonDown(Buttons.DPadDown) || (this.GamepadState.ThumbSticks.Left.Y < 0))
            {
#if DBUG
                Debug.WriteLine("Moving Down");
#endif
                return true;
            }

            return false;
        }

        /// <summary>
        /// The moving left.
        /// </summary>
        /// <returns>
        /// The moving left.
        /// </returns>
        public bool MovingLeft()
        {
            if (this.ButtonDown(Buttons.DPadLeft) || (this.GamepadState.ThumbSticks.Left.X < 0))
            {
                Debug.WriteLine("Moving Left");
                return true;
            }

            return false;
        }

        /// <summary>
        /// The moving right.
        /// </summary>
        /// <returns>
        /// The moving right.
        /// </returns>
        public bool MovingRight()
        {
            if (this.ButtonDown(Buttons.DPadRight) || (this.GamepadState.ThumbSticks.Left.X > 0))
            {
                Debug.WriteLine("Moving Right");
                return true;
            }

            return false;
        }

        /// <summary>
        /// The pressed up.
        /// </summary>
        /// <returns>
        /// The pressed up.
        /// </returns>
        public bool PressedUp()
        {
            if (this.ButtonPressed(Buttons.DPadUp))
            {
                Debug.WriteLine("Moving Up");
                return true;
            }

            return false;
        }

        /// <summary>
        /// The pressed down.
        /// </summary>
        /// <returns>
        /// The pressed down.
        /// </returns>
        public bool PressedDown()
        {
            if (this.ButtonPressed(Buttons.DPadDown))
            {
                Debug.WriteLine("Moving Down");
                return true;
            }

            return false;
        }

        /// <summary>
        /// The confirm button pressed.
        /// </summary>
        /// <returns>
        /// The confirm button pressed.
        /// </returns>
        public bool ConfirmButtonPressed()
        {
            if (this.ButtonPressed(Buttons.A))
            {
                Debug.WriteLine("Confirm Button Pressed");
                return true;
            }

            return false;
        }

        /// <summary>
        /// The pause button pressed.
        /// </summary>
        /// <returns>
        /// The pause button pressed.
        /// </returns>
        public bool PauseButtonPressed()
        {
            return this.ButtonPressed(Buttons.Start);
        }

#endif

        #endregion
    }
}