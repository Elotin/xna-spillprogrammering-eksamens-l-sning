// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DrawableSprite.cs" company="H�kon Martin Eide">
//   H�kon Martin Eide
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen.SpriteClasses
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// The DrawableSprite is the parent for 
    /// every sprite in the game.
    /// It has methods to retrieve the position, increment the position
    /// by vector or float
    /// and will take care of the drawing of the sprite itself
    /// </summary>
    public class DrawableSprite
    {
        #region Fields

        /// <summary>
        /// Sprite sheet that the NPC or player that inherits from this class will use
        /// </summary>
        private readonly Texture2D spriteSheet;

        /// <summary>
        /// The offset x.
        /// </summary>
        private int offsetX;

        /// <summary>
        /// The offset y.
        /// </summary>
        private int offsetY;

        /// <summary>
        /// The position of the sprite
        /// </summary>
        private Vector2 position;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawableSprite"/> class.
        /// </summary>
        /// <param name="spriteSheet">
        /// The sprite sheet.
        /// </param>
        public DrawableSprite(Texture2D spriteSheet)
        {
            this.spriteSheet = spriteSheet;
            this.position = Vector2.Zero;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawableSprite"/> class.
        /// </summary>
        /// <param name="spriteSheet">
        /// The sprite sheet.
        /// </param>
        /// <param name="position">
        /// The position of which to create the sprite.
        /// </param>
        public DrawableSprite(Texture2D spriteSheet, Vector2 position)
        {
            this.position = position;
            this.spriteSheet = spriteSheet;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Adds a Vector2 to the sprite position
        /// </summary>
        public Vector2 AddToPosition
        {
            set
            {
                this.position += value;
            }
        }

        /// <summary>
        /// Gets OffsetX.
        /// </summary>
        public int OffsetX
        {
            get
            {
                return this.offsetY;
            }

            private set
            {
                this.offsetX = value;
            }
        }

        /// <summary>
        /// Gets OffsetY.
        /// </summary>
        public int OffsetY
        {
            get
            {
                return this.offsetY;
            }

            private set
            {
                this.offsetY = value;
            }
        }

        /// <summary>
        /// The position of the sprite.
        /// </summary>
        public Vector2 Position
        {
            get
            {
                return this.position; // new Vector2(position.X + OffsetX, position.Y+ OffsetY);
            }

            set
            {
                this.position = value;
            }
        }

        /// <summary>
        /// Gets or sets PositionX.
        /// </summary>
        public float PositionX
        {
            get
            {
                return this.position.X;
            }

            set
            {
                this.position.X = value;
            }
        }

        /// <summary>
        /// Gets or sets PositionY.
        /// </summary>
        public float PositionY
        {
            get
            {
                return this.position.Y;
            }

            set
            {
                this.position.Y = value;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether the object has been initialized.
        /// </summary>
        protected bool IsInitialized { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the object has been Loaded.
        /// </summary>
        protected bool IsLoaded { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The draw.
        /// </summary>
        /// <param name="spriteBatch">
        /// The sprite Batch.
        /// </param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                this.spriteSheet, 
                new Vector2(this.position.X - this.OffsetX, this.position.Y - this.OffsetY), 
                Color.White);
        }

        /// <summary>
        /// The load content.
        /// </summary>
        /// <param name="content">
        /// The contentManager to be used.
        /// </param>
        public virtual void LoadContent(ContentManager content)
        {
            this.OffsetX = this.spriteSheet.Width / 2;
            this.OffsetY = this.spriteSheet.Height / 2;
        }

        /// <summary>
        /// The update method for this class
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        public virtual void Update(GameTime gameTime)
        {
        }

        #endregion
    }
}