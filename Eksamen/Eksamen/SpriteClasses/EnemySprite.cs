﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnemySprite.cs" company="Håkon Martin Eide">
//   Håkon Martin Eide
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen.SpriteClasses
{
    using System.Collections.Generic;

    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// The enemy object class.
    /// Is handled by the Enemy Manager
    /// </summary>
    public class EnemySprite : DrawableSprite
    {
        #region Fields

        /// <summary>
        /// The collision rectangles.
        /// </summary>
        private readonly List<Rectangle> collisionRectangles = new List<Rectangle>();

        /// <summary>
        /// The base speed for the enemy sprite
        /// used for calculating speed in Vertical Direction
        /// </summary>
        private readonly float speed;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EnemySprite"/> class.
        /// </summary>
        /// <param name="enemySprite">
        /// The enemy sprite.
        /// </param>
        public EnemySprite(Texture2D enemySprite)
            : base(enemySprite)
        {
            this.speed = 100;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnemySprite"/> class.
        /// </summary>
        /// <param name="enemySprite">
        /// The enemy sprite.
        /// </param>
        /// <param name="startingPosition">
        /// The starting position of the sprite if we want to declare it specificly
        /// </param>
        public EnemySprite(Texture2D enemySprite, Vector2 startingPosition)
            : base(enemySprite, startingPosition)
        {
            this.Position = startingPosition;
            this.speed = 100;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets GetCollisionRectangles.
        /// </summary>
        public List<Rectangle> GetCollisionRectangles
        {
            get
            {
                return this.collisionRectangles;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The Draw Method
        /// </summary>
        /// <param name="spriteBatch">
        /// The sprite batch.
        /// </param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        /// <summary>
        /// The content loader and initializer
        /// WIll generate the first rectangles
        /// </summary>
        /// <param name="content">
        /// The content.
        /// </param>
        public override void LoadContent(ContentManager content)
        {
            this.UpdateCollisionRectangles();
            base.LoadContent(content);
        }

        /// <summary>
        /// The update method for EnemySprite
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        public override void Update(GameTime gameTime)
        {
            var deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            Vector2 e = this.Position;
            e += new Vector2(30f * deltaTime, this.speed * deltaTime);

            if (e.Y > 520f)
            {
                e.Y = -50f;
            }

            if (e.X > 850f)
            {
                e.X = -50f;
            }

            this.Position = e;

            base.Update(gameTime);
        }

        /// <summary>
        /// This method will uppdate the collision rectangles be on target with the enemySprite
        /// It is public so that it can be called from a different class when the player is close enough to warrant a check for wether or
        /// not they may collide.
        /// The Rectangles are hardcoded because i don't know any elegant way to analyze the sprite
        /// and dynamicly generate rectangles based on the sprite.
        /// Added in the project folder is an image depicting how the different rectangles, from top to bottom,
        /// are placed on the enemy sprite.
        /// </summary>
        public void UpdateCollisionRectangles()
        {
            this.collisionRectangles.Clear();

            this.collisionRectangles.Add(
                new Rectangle((int)this.PositionX + 3 - this.OffsetX, (int)this.PositionY + 2 - this.OffsetY, 26, 9));
            this.collisionRectangles.Add(
                new Rectangle((int)this.PositionX + 6 - this.OffsetX, (int)this.PositionY + 11 - this.OffsetY, 21, 9));
            this.collisionRectangles.Add(
                new Rectangle((int)this.PositionX + 8 - this.OffsetX, (int)this.PositionY + 19 - this.OffsetY, 16, 9));
            this.collisionRectangles.Add(
                new Rectangle((int)this.PositionX + 10 - this.OffsetX, (int)this.PositionY + 28 - this.OffsetY, 12, 8));
            this.collisionRectangles.Add(
                new Rectangle((int)this.PositionX + 14 - this.OffsetX, (int)this.PositionY + 36 - this.OffsetY, 5, 26));
        }

        #endregion
    }
}