﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Player.cs" company="Håkon Martin Eide">
//   Håkon Martin Eide
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen.SpriteClasses
{
    using System.Collections.Generic;

    using Eksamen.ScreenManagerComponents;

    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// THe player object. This is the object that the player controlls.
    /// </summary>
    public class Player : DrawableSprite
    {
        #region Fields

        /// <summary>
        /// The collision rectangles.
        /// </summary>
        private readonly List<Rectangle> collisionRectangles = new List<Rectangle>();

        /// <summary>
        /// The deltatime for the game.
        /// </summary>
        private float deltaTime;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="spriteSheet">
        /// The sprite sheet for the player object.
        /// </param>
        public Player(Texture2D spriteSheet)
            : base(spriteSheet)
        {
            this.Position = new Vector2(200, 200);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="spriteSheet">
        /// The sprite sheet for the player object.
        /// </param>
        /// <param name="position">
        /// The position the player will start in.
        /// </param>
        public Player(Texture2D spriteSheet, Vector2 position)
            : base(spriteSheet, position)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets GetCollisionRectangles.
        /// </summary>
        public List<Rectangle> GetCollisionRectangles
        {
            get
            {
                return this.collisionRectangles;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The draw.
        /// </summary>
        /// <param name="spriteBatch">
        /// The sprite Batch.
        /// </param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        /// <summary>
        /// The handle input.
        /// </summary>
        /// <param name="inputHandler">
        /// The input handler.
        /// </param>
        public void HandleInput(InputHandler inputHandler)
        {
            if (inputHandler.MovingLeft())
            {
                this.PositionX -= 220f * this.deltaTime;
            }

            if (inputHandler.MovingRight())
            {
                this.PositionX += 220f * this.deltaTime;
            }

            if (inputHandler.MovingUp())
            {
                this.PositionY -= 220f * this.deltaTime;
            }

            if (inputHandler.MovingDown())
            {
                this.PositionY += 220f * this.deltaTime;
            }

            this.deltaTime = 0;
        }

        /// <summary>
        /// The load content.
        /// </summary>
        /// <param name="content">
        /// The contentManager to be used.
        /// </param>
        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);
            this.UpdateCollisionRectangles();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        public override void Update(GameTime gameTime)
        {
            this.deltaTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
        }

        /// <summary>
        /// This method will update the collision rectangles on the player object.
        /// Will be called when the player gets too close to an enemySprite for
        /// more precise collision detection
        /// The Rectangles are hardcoded because i don't know any elegant way to analyze the sprite
        /// and dynamicly generate rectangles based on the sprite.
        /// Added in the project folder is an image depicting how the different rectangles, from top to bottom,
        /// are placed on the player sprite.
        /// </summary>
        public void UpdateCollisionRectangles()
        {
            this.collisionRectangles.Clear();
            this.collisionRectangles.Add(
                new Rectangle((int)this.PositionX + 26 - this.OffsetX, (int)this.PositionY + 3 - this.OffsetY, 12, 18));
            this.collisionRectangles.Add(
                new Rectangle((int)this.PositionX + 19 - this.OffsetX, (int)this.PositionY + 23 - this.OffsetY, 26, 14));
            this.collisionRectangles.Add(
                new Rectangle((int)this.PositionX + 3 - this.OffsetX, (int)this.PositionY + 37 - this.OffsetY, 58, 13));
            this.collisionRectangles.Add(
                new Rectangle((int)this.PositionX + 19 - this.OffsetX, (int)this.PositionY + 50 - this.OffsetY, 26, 6));
        }

        #endregion
    }
}