// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Game1.cs" company="H�kon Martin Eide">
//   H�kon Martin Eide
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen
{
    using Eksamen.GameScreens.MenuScreens;
    using Eksamen.ScreenManagerComponents;

    using Microsoft.Xna.Framework;

    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        #region Fields

        /// <summary>
        /// The screenManager for the game, will handle all screen transitions and states.
        /// </summary>
        private readonly ScreenManager screenManager;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Game1"/> class.
        /// </summary>
        public Game1()
        {
            new GraphicsDeviceManager(this);
            this.Content.RootDirectory = "Content";

            this.screenManager = new ScreenManager(this);
            this.Components.Add(this.screenManager);
            this.screenManager.AddScreen(new MainMenuScreen(this.screenManager));
        }

        #endregion

        #region Methods

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">
        /// Provides a snapshot of timing values.
        /// </param>
        protected override void Draw(GameTime gameTime)
        {
            this.GraphicsDevice.Clear(Color.CornflowerBlue);

            base.Draw(gameTime);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">
        /// Provides a snapshot of timing values.
        /// </param>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        #endregion
    }
}