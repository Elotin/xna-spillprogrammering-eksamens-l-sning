﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScoreTracker.cs" company="Håkon Martin Eide">
//   Håkon Martin Eide
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen.GameScreens
{
    using System;

    /// <summary>
    /// Singleton used to handle the GameScore
    /// </summary>
    public sealed class ScoreTracker
    {
        #region Static Fields

        /// <summary>
        /// The instance.
        /// </summary>
        private static readonly ScoreTracker instance = new ScoreTracker();

        #endregion

        #region Fields

        /// <summary>
        /// The current score.
        /// </summary>
        private double currentScore;

        /// <summary>
        /// The high score.
        /// </summary>
        private double highScore;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Prevents a default instance of the <see cref="ScoreTracker"/> class from being created.
        /// </summary>
        private ScoreTracker()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets Instance.
        /// </summary>
        public static ScoreTracker Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// Gets or sets CurrentScore.
        /// </summary>
        public double CurrentScore
        {
            get
            {
                return this.currentScore;
            }

            set
            {
                this.currentScore = value;
            }
        }

        /// <summary>
        /// Gets GetHighScore.
        /// </summary>
        public double GetHighScore
        {
            get
            {
                return this.highScore;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The stop tracking.
        /// </summary>
        public void SaveScore()
        {
            if (this.currentScore > this.highScore)
            {
                currentScore = Math.Round(currentScore, 2);
                this.highScore = this.currentScore;
            }
        }
        #endregion
    }
}