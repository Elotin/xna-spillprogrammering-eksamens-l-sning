﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScreenTemplate.cs" company="Håkon Martin Eide">
//   Håkon Martin Eide
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen.GameScreens
{
    using System;

    using Eksamen.ScreenManagerComponents;

    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// The screen template.
    /// </summary>
    internal class ScreenTemplate : GameScreen
    {
        #region Fields

        /// <summary>
        /// The contentManager. Will get its value from the screenManager.
        /// </summary>
        private ContentManager content;

        /// <summary>
        /// The default game font.
        /// </summary>
        private SpriteFont gameFont;

        /// <summary>
        /// The pause alpha.
        /// </summary>
        private float pauseAlpha;

        /// <summary>
        /// The sprite batch.
        /// </summary>
        private SpriteBatch spriteBatch;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ScreenTemplate"/> class.
        /// </summary>
        /// <param name="screenManager">
        /// The screen manager.
        /// </param>
        public ScreenTemplate(ScreenManager screenManager)
        {
            this.TransitionOnTime = TimeSpan.FromSeconds(1.5);
            this.TransitionOffTime = TimeSpan.FromSeconds(0.5);
            this.spriteBatch = screenManager.SpriteBatch;
            this.ScreenManager = screenManager;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The draw method.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        public override void Draw(GameTime gameTime)
        {
            this.spriteBatch = this.ScreenManager.SpriteBatch;
            this.ScreenManager.GraphicsDevice.Clear(Color.CornflowerBlue);

            this.ScreenManager.SpriteBatch.Begin();

            this.ScreenManager.SpriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (this.TransitionPosition > 0 || this.pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - this.TransitionAlpha, 1f, this.pauseAlpha / 2);

                this.ScreenManager.FadeBackToBlack(alpha);
            }
        }

        /// <summary>
        /// Called from the ScreenManager. Will handle input for the selected screen
        /// 
        /// </summary>
        /// <param name="input">
        /// </param>
        public override void HandleInput(InputHandler input)
        {
            base.HandleInput(input);
        }

        /// <summary>
        /// Initializes the game screen
        /// </summary>
        public override void Initialize()
        {
        }

        /// <summary>
        /// The load content.
        /// </summary>
        public override void LoadContent()
        {
            if (this.content == null)
            {
                this.content = this.ScreenManager.Content;
            }

            this.gameFont = this.content.Load<SpriteFont>("font");
        }

        /// <summary>
        /// The unload content.
        /// </summary>
        public override void UnloadContent()
        {
            this.content.Unload();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        /// <param name="otherScreenHasFocus">
        /// The other screen has focus.
        /// </param>
        /// <param name="coveredByOtherScreen">
        /// The covered by other screen.
        /// </param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            this.pauseAlpha = coveredByOtherScreen
                                  ? Math.Min(this.pauseAlpha + 1f / 32, 1)
                                  : Math.Max(this.pauseAlpha - 1f / 32, 0);

            if (this.IsActive)
            {
            }
        }

        #endregion
    }
}