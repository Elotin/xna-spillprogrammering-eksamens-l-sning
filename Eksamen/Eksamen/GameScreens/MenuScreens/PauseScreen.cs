﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PauseScreen.cs" company="Håkon Martin Eide">
//   Håkon Martin Eide
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen.GameScreens.MenuScreens
{
    using System;
    using System.Collections.Generic;

    using Eksamen.ScreenManagerComponents;

    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;

    /// <summary>
    /// The screen template.
    /// </summary>
    internal class PauseScreen : GameScreen
    {
        #region Fields

        /// <summary>
        /// The contentManager. Will get its value from the screenManager.
        /// </summary>
        private ContentManager content;

        /// <summary>
        /// The default game font.
        /// </summary>
        private SpriteFont gameFont;

        /// <summary>
        /// The pause alpha.
        /// </summary>
        private float pauseAlpha;

        private Texture2D background;

        /// <summary>
        /// The MenuItems list will contain the strings used to generate the menu selections
        /// </summary>
        private readonly List<String> menuItems = new List<string>();

        /// <summary>
        /// The selectedMenuEntry is an int that stores where in the MenuItems list
        /// we are currently selecting
        /// </summary>
        private int selectedMenuEntry = 1;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ScreenTemplate"/> class.
        /// </summary>
        /// <param name="screenManager">
        /// The screen manager.
        /// </param>
        public PauseScreen(ScreenManager screenManager)
        {
            this.TransitionOnTime = TimeSpan.FromSeconds(0.5);
            this.TransitionOffTime = TimeSpan.FromSeconds(0.0);
            this.ScreenManager = screenManager;

            this.IsPopup = true;

            this.menuItems.Add("Continue");
            this.menuItems.Add("Menu");
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The draw method.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        public override void Draw(GameTime gameTime)
        {
            this.ScreenManager.SpriteBatch.Begin();

            this.ScreenManager.SpriteBatch.Draw(this.background, Vector2.Zero, Color.Black);

            for (int i = 0; i < 2; i++)
            {
                Color spriteFontColor = this.selectedMenuEntry == i ? Color.Black : Color.White;
                this.ScreenManager.SpriteBatch.DrawString(this.gameFont, this.menuItems[i], new Vector2(350, 241 + 50 * i), spriteFontColor);
            }

            this.ScreenManager.SpriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (this.TransitionPosition > 0 || this.pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - this.TransitionAlpha, 1f, this.pauseAlpha / 2);

                this.ScreenManager.FadeBackToBlack(alpha);
            }
        }

        /// <summary>
        /// Called from the ScreenManager. Will handle input for the selected screen
        /// 
        /// </summary>
        /// <param name="input">
        /// </param>
        public override void HandleInput(InputHandler input)
        {
            if (input.KeyPressed(Keys.Down))
            {
                this.selectedMenuEntry = this.selectedMenuEntry == 0 ? 1 : 0;
            }

            if (input.KeyPressed(Keys.Up))
            {
                this.selectedMenuEntry = this.selectedMenuEntry == 1 ? 0 : 1;
            }

            if (input.ConfirmButtonPressed())
            {
                this.MenuItemPressed();
            }
        }
        /// <summary>
        /// Initializes the game screen
        /// </summary>
        public override void Initialize()
        {
        }
        /// <summary>
        /// The load content.
        /// </summary>
        public override void LoadContent()
        {
            if (this.content == null)
            {
                this.content = this.ScreenManager.Content;
            }

            this.background = this.content.Load<Texture2D>("menuBG");
            this.gameFont = this.content.Load<SpriteFont>("font");
        }

        /// <summary>
        /// The unload content.
        /// </summary>
        public override void UnloadContent()
        {
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        /// <param name="otherScreenHasFocus">
        /// The other screen has focus.
        /// </param>
        /// <param name="coveredByOtherScreen">
        /// The covered by other screen.
        /// </param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            this.pauseAlpha = coveredByOtherScreen
                                  ? Math.Min(this.pauseAlpha + 1f / 32, 1)
                                  : Math.Max(this.pauseAlpha - 1f / 32, 0);

            if (this.IsActive)
            {
            }
        }

        #endregion
        #region private
        private void MenuItemPressed()
        {
            if (this.selectedMenuEntry == 1)
                this.ExitScreen();
            if (this.selectedMenuEntry == 0)
                this.ScreenManager.ReturnToMenu();
        }
        #endregion
    }
}