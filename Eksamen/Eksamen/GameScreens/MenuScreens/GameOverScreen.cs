﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameOverScreen.cs" company="Håkon Martin Eide">
//   Håkon Martin Eide
// </copyright>
// <summary>
//   The game over screen. Will just tell you that you lost and then return you to the menu
//   after a couple seconds or you press continue
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen.GameScreens.MenuScreens
{
    using System;
    using System.Collections.Generic;

    using Eksamen.ScreenManagerComponents;

    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// The game over screen. Will just tell you that you lost and then return you to the menu
    /// after a couple seconds or you press continue
    /// </summary>
    internal class GameOverScreen : GameScreen
    {
        #region Fields

        /// <summary>
        /// The MenuItems list will contain the strings used to generate the menu selections
        /// </summary>
        private readonly List<string> menuItems = new List<string>();

        /// <summary>
        /// The contentManager. Will get its value from the screenManager.
        /// </summary>
        private ContentManager content;

        /// <summary>
        /// The default game font.
        /// </summary>
        private SpriteFont gameFont;

        /// <summary>
        /// The pause alpha.
        /// </summary>
        private float pauseAlpha;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GameOverScreen"/> class.
        /// </summary>
        /// <param name="screenManager">
        /// The screen manager.
        /// </param>
        public GameOverScreen(ScreenManager screenManager)
        {
            this.TransitionOnTime = TimeSpan.FromSeconds(0.5);
            this.TransitionOffTime = TimeSpan.FromSeconds(0.0);
            this.ScreenManager = screenManager;

            this.IsPopup = true;

            this.menuItems.Add("Continue");
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The draw method.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        public override void Draw(GameTime gameTime)
        {
            this.ScreenManager.SpriteBatch.Begin();

            this.ScreenManager.SpriteBatch.DrawString(
                this.gameFont, 
                "You Lost", 
                new Vector2(280, 100), 
                Color.Black, 
                0f, 
                Vector2.Zero, 
                3f, 
                SpriteEffects.None, 
                0f);

            this.ScreenManager.SpriteBatch.DrawString(
                this.gameFont, this.menuItems[0], new Vector2(350, 241), Color.White);

            this.ScreenManager.SpriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (this.TransitionPosition > 0 || this.pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - this.TransitionAlpha, 1f, this.pauseAlpha / 2);

                this.ScreenManager.FadeBackToBlack(alpha);
            }
        }

        /// <summary>
        /// Called from the ScreenManager. Will handle input for the selected screen
        /// 
        /// </summary>
        /// <param name="input">
        /// </param>
        public override void HandleInput(InputHandler input)
        {
            if (input.ConfirmButtonPressed())
            {
                this.MenuItemPressed();
            }
        }

        /// <summary>
        /// Initializes the game screen
        /// </summary>
        public override void Initialize()
        {
        }

        /// <summary>
        /// The load content.
        /// </summary>
        public override void LoadContent()
        {
            if (this.content == null)
            {
                this.content = this.ScreenManager.Content;
            }

            this.gameFont = this.content.Load<SpriteFont>("font");
        }

        /// <summary>
        /// The unload content.
        /// </summary>
        public override void UnloadContent()
        {
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        /// <param name="otherScreenHasFocus">
        /// The other screen has focus.
        /// </param>
        /// <param name="coveredByOtherScreen">
        /// The covered by other screen.
        /// </param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            this.pauseAlpha = coveredByOtherScreen
                                  ? Math.Min(this.pauseAlpha + 1f / 32, 1)
                                  : Math.Max(this.pauseAlpha - 1f / 32, 0);

            if (this.IsActive)
            {
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The menu item pressed.
        /// </summary>
        private void MenuItemPressed()
        {
            this.ScreenManager.ReturnToMenu();
        }

        #endregion
    }
}