﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameScreen.cs" company="Håkon Martin Eide">
//   Håkon Martin Eide
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen.GameScreens
{
    using System;

    using Eksamen.GameScreens.MenuScreens;
    using Eksamen.ScreenManagerComponents;

    using Microsoft.Xna.Framework;

    /// <summary>
    /// The ScreenState enums are there to give each screen a state.
    /// A screen can have only one state, and which state it is in will determine
    /// how it behaves and which methods are called.
    /// </summary>
    public enum ScreenState
    {
        /// <summary>
        /// The transition off.
        /// </summary>
        TransitionOff, 

        /// <summary>
        /// The active.
        /// </summary>
        Active, 

        /// <summary>
        /// The transition on.
        /// </summary>
        TransitionOn, 

        /// <summary>
        /// The hidden.
        /// </summary>
        Hidden, 
    }

    /// <summary>
    /// The GameScreen class is a parent class for all screens in the game.
    /// It has vital methods for handling screen transitions, updates, draws and input handles.
    /// It integrates fully with the screen manager and provides easy calls for different actions
    /// you may need on your screen
    /// </summary>
    public abstract class GameScreen
    {
        #region Fields

        /// <summary>
        /// The is exiting.
        /// </summary>
        private bool isExiting;

        /// <summary>
        /// The other screen has focus.
        /// </summary>
        private bool otherScreenHasFocus;

        /// <summary>
        /// When the object is created, it will always be added to the screen manager.
        /// The screen manager will make this the active screen and it will therefore be in the
        /// TransitionOn state.
        /// </summary>
        private ScreenState screenState = ScreenState.TransitionOn;

        /// <summary>
        /// The transition off time.
        /// </summary>
        private TimeSpan transitionOffTime = TimeSpan.Zero;

        /// <summary>
        /// The transition on time.
        /// </summary>
        private TimeSpan transitionOnTime = TimeSpan.Zero;

        /// <summary>
        /// The transition position.
        /// </summary>
        private float transitionPosition = 1;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GameScreen"/> class.
        /// </summary>
        protected GameScreen()
        {
            this.IsPopup = false;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether IsActive.
        /// </summary>
        public bool IsActive
        {
            get
            {
                return !this.otherScreenHasFocus
                       && (this.screenState == ScreenState.TransitionOn || this.screenState == ScreenState.Active);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsExiting.
        /// </summary>
        public bool IsExiting
        {
            get
            {
                return this.isExiting;
            }

            protected internal set
            {
                this.isExiting = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsPopup.
        /// Being a popup means its a screen that covers another temporarily
        /// and will not cover up. This means that the background will
        /// be transparant and the screen directly under it will continue to
        /// be drawn.
        /// </summary>
        public bool IsPopup { get; protected set; }

        /// <summary>
        /// Gets or sets ScreenManager.
        /// </summary>
        public ScreenManager ScreenManager { get; internal set; }

        /// <summary>
        /// Gets or sets ScreenState.
        /// </summary>
        public ScreenState ScreenState
        {
            get
            {
                return this.screenState;
            }

            protected internal set
            {
                this.screenState = value;
            }
        }

        /// <summary>
        /// Gets TransitionAlpha.
        /// How transparant the background will be in different situations.
        /// </summary>
        public float TransitionAlpha
        {
            get
            {
                return 1f - this.TransitionPosition;
            }
        }

        /// <summary>
        /// Gets or sets TransitionOffTime.
        /// This determines how long the screen will take to transition off.
        /// </summary>
        public TimeSpan TransitionOffTime
        {
            get
            {
                return this.transitionOffTime;
            }

            protected set
            {
                this.transitionOffTime = value;
            }
        }

        /// <summary>
        /// Gets or sets TransitionOnTime.
        /// This determines how long the screen will take to transition on.
        /// </summary>
        public TimeSpan TransitionOnTime
        {
            get
            {
                return this.transitionOnTime;
            }

            protected set
            {
                this.transitionOnTime = value;
            }
        }

        /// <summary>
        /// Gets or sets TransitionPosition.
        /// Gets or Sets how far the screen is in its Transition
        /// process
        /// </summary>
        public float TransitionPosition
        {
            get
            {
                return this.transitionPosition;
            }

            protected set
            {
                this.transitionPosition = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The draw.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        public virtual void Draw(GameTime gameTime)
        {
        }

        /// <summary>
        /// The exit screen.
        /// This one will check if we have any transitionofftime.
        /// If we don't then it will exit immediatly.
        /// If we do then it will be set in the isExiting state
        /// and start to transitionoff.
        /// </summary>
        public void ExitScreen()
        {
            if (this.transitionOffTime == TimeSpan.Zero)
            {
                this.ScreenManager.RemoveScreen(this);
            }
            else
            {
                this.isExiting = true;
            }
        }

        /// <summary>
        /// WIll handle input shared across screens
        /// Useful for actions such as pausing and exit.
        /// Can be "Disabled" by not calling method from the sub-class.
        /// </summary>
        /// <param name="input">
        /// The inputHandler to be used.
        /// </param>
        public virtual void HandleInput(InputHandler input)
        {
            if (input.PauseButtonPressed())
            {
                ScreenManager.AddScreen(new PauseScreen(ScreenManager));
            }
        }

        /// <summary>
        /// Initialize
        /// </summary>
        public virtual void Initialize()
        {
        }

        /// <summary>
        /// The load content.
        /// </summary>
        public virtual void LoadContent()
        {
        }

        /// <summary>
        /// The unload content.
        /// </summary>
        public virtual void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">
        /// Provides a snapshot of timing values.
        /// </param>
        /// <param name="otherScreenHasFocus">
        /// If otherScreenHasFocus is true it means we've tabbed away from our application
        /// and the update will be paused until it regains focus.
        /// </param>
        /// <param name="coveredByOtherScreen">
        /// If we are covered by another screen that is not
        /// a popup, it will update its transition time or if the transition is
        /// complete, put itself in the hidden state
        /// </param>
        public virtual void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            this.otherScreenHasFocus = otherScreenHasFocus;

            if (this.isExiting)
            {
                this.screenState = ScreenState.TransitionOff;

                if (!this.UpdateTransition(gameTime, this.transitionOffTime, 1))
                {
                    this.ScreenManager.RemoveScreen(this);
                }
            }
            else if (coveredByOtherScreen)
            {
                this.screenState = this.UpdateTransition(gameTime, this.transitionOffTime, 1)
                                       ? ScreenState.TransitionOff
                                       : ScreenState.Hidden;
            }
            else
            {
                this.screenState = this.UpdateTransition(gameTime, this.transitionOffTime, -1)
                                       ? ScreenState.TransitionOn
                                       : ScreenState.Active;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The update transition.
        /// Will update the transitioning on or off screen.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        /// <param name="time">
        /// The time.
        /// </param>
        /// <param name="direction">
        /// The direction.
        /// </param>
        /// <returns>
        /// The update transition.
        /// </returns>
        private bool UpdateTransition(GameTime gameTime, TimeSpan time, int direction)
        {
            // Hvor langt skal transition bevege seg
            float transitionDelta;

            if (time == TimeSpan.Zero)
            {
                transitionDelta = 1;
            }
            else
            {
                transitionDelta = (float)(gameTime.ElapsedGameTime.TotalMilliseconds / time.TotalMilliseconds);
            }

            // Bevege transition
            this.transitionPosition += transitionDelta * direction;

            // Er vi ferdig med transition?
            if (((direction < 0) && (this.transitionPosition <= 0))
                || ((direction > 0) && (this.transitionPosition >= 1)))
            {
                this.transitionPosition = MathHelper.Clamp(this.transitionPosition, 0, 1);
                return false;
            }

            return true;
        }

        #endregion
    }
}