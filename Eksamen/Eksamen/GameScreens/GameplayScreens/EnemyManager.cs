﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnemyManager.cs" company="Håkon Martin Eide">
//   Håkon Martin Eide
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen.GameScreens.GameplayScreens
{
    using System;
    using System.Collections.Generic;

    using Eksamen.SpriteClasses;

    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// Enemy Manager. Will handle spawning and recycling of enemies and their collisions
    /// </summary>
    public class EnemyManager
    {
        #region Fields

        /// <summary>
        /// Random seed for position generation
        /// </summary>
        private readonly Random random = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// The list that will contain all enemies spawned
        /// </summary>
        private readonly List<EnemySprite> enemies = new List<EnemySprite>();

        /// <summary>
        /// The texture for the enemies
        /// </summary>
        private readonly Texture2D enemyTexture;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EnemyManager"/> class.
        /// </summary>
        /// <param name="enemyTexture">
        /// The enemy texture.
        /// </param>
        public EnemyManager(Texture2D enemyTexture)
        {
            this.enemyTexture = enemyTexture;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets GetEnemySpriteList.
        /// </summary>
        public List<EnemySprite> GetEnemySpriteList
        {
            get
            {
                return this.enemies;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The draw method for the enemyManager.
        /// Will Iterate through every enemy and call its draw method
        /// </summary>
        /// <param name="spriteBatch">
        /// The sprite batch.
        /// </param>
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (EnemySprite enemyToDraw in this.enemies)
            {
                enemyToDraw.Draw(spriteBatch);
            }
        }

        /// <summary>
        /// The load content.
        /// </summary>
        /// <param name="content">
        /// The content loader, used for populating the enemy list
        /// </param>
        public void LoadContent(ContentManager content)
        {
            // Adds the enemies to the enemies list and gives them their random starting position and speed
            for (int creatingEnemy = 0; creatingEnemy < 10; creatingEnemy++)
            {
                this.enemies.Add(
                    new EnemySprite(
                        this.enemyTexture, 
                        new Vector2(
                            (900f * (float)this.random.NextDouble()) - 50f, 
                            -50f - 400f * (float)this.random.NextDouble())));
            }

            foreach (EnemySprite spriteToLoad in this.enemies)
            {
                spriteToLoad.LoadContent(content);
            }
        }

        /// <summary>
        /// The update method for enemyManager
        /// Will iterate through every enemy and call its update method
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        public void Update(GameTime gameTime)
        {
            foreach (EnemySprite enemyToUpdate in this.enemies)
            {
                enemyToUpdate.Update(gameTime);
            }
        }

        #endregion
    }
}