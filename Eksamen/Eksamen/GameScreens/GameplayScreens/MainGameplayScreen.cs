﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainGameplayScreen.cs" company="Håkon Martin Eide">
//   Håkon Martin Eide
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen.GameScreens.GameplayScreens
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using Eksamen.GameScreens.MenuScreens;
    using Eksamen.ScreenManagerComponents;
    using Eksamen.SpriteClasses;

    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// Main Gameplay Screen
    /// </summary>
    internal class MainGameplayScreen : GameScreen
    {
        #region Fields

        /// <summary>
        /// The list containing all objects inheriting from the DrawableSprite class.
        /// Used to cycle through and call updates and draws for the respective objects
        /// </summary>
        private readonly List<DrawableSprite> drawableSpritesList = new List<DrawableSprite>();

        /// <summary>
        /// The collision checker.
        /// </summary>
        private CollisionChecker collisionChecker;

        /// <summary>
        /// The contentManager. Will get its value from the screenManager.
        /// </summary>
        private ContentManager content;

        /// <summary>
        /// The enemyManager object
        /// Used to handle the enemies on the screen.
        /// </summary>
        private EnemyManager enemyManager;

        /// <summary>
        /// The enemy sprites.
        /// This list is gathered from the sprite manager and is 
        /// used to retrive information regarding position and
        /// their collosion rectangles for collision checks
        /// </summary>
        private List<EnemySprite> enemySprites = new List<EnemySprite>();

        /// <summary>
        /// The sprite for the Enemy
        /// </summary>
        private Texture2D enemyTexture2D;

        /// <summary>
        /// The default game font.
        /// </summary>
        private SpriteFont gameFont;

        /// <summary>
        /// The pause alpha.
        /// </summary>
        private float pauseAlpha;

        /// <summary>
        /// The player object
        /// </summary>
        private Player player;

        /// <summary>
        /// The sprite for the player
        /// </summary>
        private Texture2D playerTexture2D;

        /// <summary>
        /// The sprite batch.
        /// </summary>
        private readonly SpriteBatch spriteBatch;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MainGameplayScreen"/> class.
        /// </summary>
        /// <param name="screenManager">
        /// The screen manager.
        /// </param>
        public MainGameplayScreen(ScreenManager screenManager)
        {
            this.TransitionOnTime = TimeSpan.FromSeconds(1.5);
            this.TransitionOffTime = TimeSpan.FromSeconds(0.5);
            this.spriteBatch = screenManager.SpriteBatch;
            this.ScreenManager = screenManager;

            ScoreTracker.Instance.CurrentScore = 0;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The draw method.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        public override void Draw(GameTime gameTime)
        {
            this.ScreenManager.GraphicsDevice.Clear(Color.CornflowerBlue);

            ScreenManager.SpriteBatch.Begin();

            foreach (DrawableSprite spriteToDraw in this.drawableSpritesList)
            {
                spriteToDraw.Draw(ScreenManager.SpriteBatch);
            }

            this.enemyManager.Draw(this.spriteBatch);

            this.ScreenManager.SpriteBatch.DrawString(gameFont, "Current Score: " + ScoreTracker.Instance.CurrentScore.ToString(), new Vector2(550, 10), Color.Black);
            this.ScreenManager.SpriteBatch.DrawString(gameFont, "Current Score: " + ScoreTracker.Instance.GetHighScore.ToString(), new Vector2(10, 10), Color.Black);

            ScreenManager.SpriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (this.TransitionPosition > 0 || this.pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - this.TransitionAlpha, 1f, this.pauseAlpha / 2);

                this.ScreenManager.FadeBackToBlack(alpha);
            }
        }

        /// <summary>
        /// Called from the ScreenManager. Will handle input for the selected screen
        /// </summary>
        /// <param name="input">
        /// The input handler passed from ScreenManager
        /// Has predefined checks for certain actions like directions and accept/decline
        /// </param>
        public override void HandleInput(InputHandler input)
        {
            if (this.player != null)
            {
                this.player.HandleInput(input);
            }

            base.HandleInput(input);
        }

        /// <summary>
        /// Initializes the game screen
        /// </summary>
        public override void Initialize()
        {
        }

        /// <summary>
        /// The load content.
        /// </summary>
        public override void LoadContent()
        {
            Debug.WriteLine("Screen content loaded");
            if (this.content == null)
            {
                this.content = this.ScreenManager.Content;
            }

            this.playerTexture2D = this.content.Load<Texture2D>("player");
            this.enemyTexture2D = this.content.Load<Texture2D>("enemy");
            this.gameFont = this.content.Load<SpriteFont>("Font");

            this.player = new Player(this.playerTexture2D);
            this.enemyManager = new EnemyManager(this.enemyTexture2D);

            this.drawableSpritesList.Add(this.player);

            this.collisionChecker = new CollisionChecker();

            foreach (DrawableSprite drawableSprite in this.drawableSpritesList)
            {
                drawableSprite.LoadContent(this.content);
            }

            this.enemyManager.LoadContent(this.content);
        }

        /// <summary>
        /// The unload content.
        /// </summary>
        public override void UnloadContent()
        {
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="gameTime">
        /// The game time.
        /// </param>
        /// <param name="otherScreenHasFocus">
        /// The other screen has focus.
        /// </param>
        /// <param name="coveredByOtherScreen">
        /// The covered by other screen.
        /// </param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            this.pauseAlpha = coveredByOtherScreen
                                  ? Math.Min(this.pauseAlpha + 1f / 32, 1)
                                  : Math.Max(this.pauseAlpha - 1f / 32, 0);

            if (this.IsActive)
            {
                ScoreTracker.Instance.CurrentScore += Math.Round(gameTime.ElapsedGameTime.TotalSeconds * 5, 2);

                foreach (DrawableSprite drawableSprite in this.drawableSpritesList)
                {
                    drawableSprite.Update(gameTime);
                }

                this.enemyManager.Update(gameTime);

                if (this.Collisioncheck())
                {
                    ScoreTracker.Instance.SaveScore();
                    ScreenManager.AddScreen(new GameOverScreen(ScreenManager));
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method will get the enemy sprite list.
        /// Check if we are close to colliding with any of them.
        /// If we are it will update the collision rectangle for the potential colliding 
        /// enemy and the player.
        /// It will then send these lists to the collision checker object.
        /// The collisionChecker object will then determine if the rectangles are intersecting and return true.
        /// We then return this result.
        /// </summary>
        /// <returns>
        /// The collisioncheck.
        /// </returns>
        private bool Collisioncheck()
        {
            this.enemySprites = this.enemyManager.GetEnemySpriteList;

            foreach (EnemySprite enemySprite in this.enemySprites)
            {
                if (Vector2.Distance(this.player.Position, enemySprite.Position) < 50)
                {
                    enemySprite.UpdateCollisionRectangles();
                    this.player.UpdateCollisionRectangles();

                    List<Rectangle> enemySpriteRectangles = enemySprite.GetCollisionRectangles;
                    List<Rectangle> playerCollisionRectangles = this.player.GetCollisionRectangles;

                    this.collisionChecker.SetRectangleList1 = playerCollisionRectangles;
                    this.collisionChecker.SetRectangleList2 = enemySpriteRectangles;

                    return this.collisionChecker.CheckCollision();
                }
            }

            return false;
        }

        #endregion
    }
}