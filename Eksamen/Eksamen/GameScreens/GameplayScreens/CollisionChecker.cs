﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CollisionChecker.cs" company="Håkon Martin Eide">
//   Håkon Martin Eide
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Eksamen.GameScreens.GameplayScreens
{
    using System.Collections.Generic;

    using Microsoft.Xna.Framework;

    /// <summary>
    /// The collision checkers purpose is to 
    /// get two lists of rectangles.
    /// Check if they intersects and return a boolean value
    /// </summary>
    public class CollisionChecker
    {
        #region Fields

        /// <summary>
        /// The rectangle list 1.
        /// </summary>
        public List<Rectangle> RectangleList1;

        /// <summary>
        /// The rectangle list 2.
        /// </summary>
        public List<Rectangle> RectangleList2;

        #endregion

        #region Public Properties

        /// <summary>
        /// Sets SetRectangleList1.
        /// </summary>
        public List<Rectangle> SetRectangleList1
        {
            set
            {
                this.RectangleList1 = value;
            }
        }

        /// <summary>
        /// Sets SetRectangleList2.
        /// </summary>
        public List<Rectangle> SetRectangleList2
        {
            set
            {
                this.RectangleList2 = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The Collision check
        /// </summary>
        /// <returns>
        /// The check initial collision.
        /// </returns>
        public bool CheckCollision()
        {
            foreach (Rectangle rectangleFromList2 in this.RectangleList2)
            {
                foreach (Rectangle rectangleFromList1 in this.RectangleList1)
                {
                    if (rectangleFromList2.Intersects(rectangleFromList1))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion
    }
}